<?php

namespace Drupal\Tests\entity_data\Kernel;

use Drupal\Core\Entity\EntityInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Test entity data service functionality.
 *
 * @group entity_data
 */
class EntityDataTest extends EntityKernelTestBase {

  /**
   * Entity data.
   *
   * @var \Drupal\entity_data\EntityDataInterface
   */
  protected $entityData;

  /**
   * Immutable configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();

    $this->installConfig(['node', 'entity_data']);
    $this->installSchema('entity_data', ['entity_data']);
    $this->installSchema('node', ['node_access']);

    $this->installEntitySchema('node');

    $this->createNodeType('page');

    $this->entityData = $this->container->get('entity.data');
    $this->config = $this->container->get('config.factory')->getEditable('entity_data.settings');
  }

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'entity_data',
  ];

  /**
   * Test get, set, delete methods.
   */
  public function testEntityDataGetSet() {
    $module_name = 'mymodule';
    $entity_id = 1;
    $entity_type = 'node';
    $data_name = 'my_custom_setting';
    $value = 'my value';
    $this->entityData->set($module_name, $entity_id, $data_name, $entity_type, $value);

    $stored_value = $this->entityData->get($module_name, $entity_id, $data_name, $entity_type);
    $this->assertEquals($value, $stored_value);

    $this->entityData->delete($module_name, $entity_id, $data_name, $entity_type);
  }

  /**
   * Test data fetching.
   */
  public function testEntityDataGetSetMultipleValues() {
    $module_name = 'mymodule';
    $entity_type_node = 'node';
    $entity_type_term = 'taxonomy_term';
    $data_name = 'my_custom_setting';
    $value = 'my value';
    $count_term = 3;
    for ($i = 1; $i <= $count_term; $i++) {
      $this->entityData->set($module_name, $i, $data_name, $entity_type_term, $value);
    }
    $count_node = 2;
    for ($i = 1; $i <= $count_node; $i++) {
      $this->entityData->set($module_name, $i, $data_name, $entity_type_node, $value);
    }
    $another_module_name = 'mymodule2';
    $another_data_name = 'my_custom_setting2';
    $another_count = 2;
    for ($i = 1; $i <= $another_count; $i++) {
      $this->entityData->set($another_module_name, $i, $another_data_name, $entity_type_node, $value);
    }

    // Get entity data per module.
    $result = $this->entityData->get($module_name);

    $this->assertEquals($count_term + $count_node, count($result[$entity_type_term]) + count($result[$entity_type_node]));

    $result = $this->entityData->get($another_module_name);
    $this->assertEquals($another_count, count($result[$entity_type_node]));

    // Get entity data per entity type.
    $result = $this->entityData->get($module_name, NULL, NULL, $entity_type_term);

    $this->assertEquals($count_term, count($result));

    $result = $this->entityData->get($module_name, NULL, NULL, $entity_type_node);
    $this->assertEquals($count_node, count($result));

    // Get entity data per entity id.
    $result = $this->entityData->get($module_name, 1);

    $this->assertEquals(2, count($result));

    $result = $this->entityData->get($module_name, 2);
    $this->assertEquals(2, count($result));

    $result = $this->entityData->get($module_name, 3);
    $this->assertEquals(1, count($result));

    // Get entity data specific value.
    $result = $this->entityData->get($module_name, 1, $data_name, $entity_type_node);
    $this->assertEquals($value, $result);

    // Get entity data by name and entity type.
    $result = $this->entityData->get($module_name, NULL, $data_name, $entity_type_node);
    $this->assertEquals($count_node, count($result[$entity_type_node]));

    $result = $this->entityData->get($module_name, NULL, $data_name, $entity_type_term);
    $this->assertEquals($count_term, count($result[$entity_type_term]));

    // Get entity data by name.
    $result = $this->entityData->get($module_name, NULL, $data_name, NULL);
    $this->assertEquals($count_node + $count_term, count($result[$entity_type_term]) + count($result[$entity_type_node]));

    $result = $this->entityData->get($module_name, NULL, $another_data_name, NULL);
    $this->assertEquals(0, count($result));

    $result = $this->entityData->get($another_module_name, NULL, $another_data_name, NULL);
    $this->assertEquals($another_count, count($result[$entity_type_node]));
  }

  /**
   * Test saving of objects and allowed_classes settings.
   */
  public function testEntityDataSaveObject() {
    $module_name = 'mymodule';
    $entity_type = 'node';
    $data_name = 'some data';
    $entity_id = 1;

    $node = $this->createNode('page');
    $this->entityData->set($module_name, $entity_id, $data_name, $entity_type, $node);

    $node_class = get_class($node);
    // By default classes are not allowed.
    $stored_node = $this->entityData->get($module_name, $entity_id, $data_name, $entity_type);
    $this->assertNotEquals($node_class, get_class($stored_node));

    $this->config->set('are_classes_allowed', TRUE);
    $this->config->set('allowed_classes', ['Drupal\taxonomy\Entity\Term']);
    $this->config->save();

    // Try to unserialize not allowed class.
    $stored_node = $this->entityData->get($module_name, $entity_id, $data_name, $entity_type);
    $this->assertNotEquals($node_class, get_class($stored_node));

    $this->config->set('allowed_classes', ['Drupal\node\Entity\Node']);
    $this->config->save();

    $stored_node = $this->entityData->get($module_name, $entity_id, $data_name, $entity_type);
    $this->assertEquals($node_class, get_class($stored_node));
  }

  /**
   * Test clean up entity data, when entity is deleted.
   */
  public function testCleanUpEntityData() {
    $module_name = 'mymodule';
    $data_name = 'some data';
    $value = 'some value';

    $node = $this->createNode('page');
    $this->entityData->set($module_name, $node->id(), $data_name, $node->getEntityTypeId(), $value);

    $stored_value = $this->entityData->get($module_name, $node->id(), $data_name, $node->getEntityTypeId());
    $this->assertEquals($stored_value, $stored_value);

    $this->removeEntity($node);
    $stored_value = $this->entityData->get($module_name, $node->id(), $data_name, $node->getEntityTypeId());
    $this->assertNull($stored_value);
  }

  /**
   * Create an entity.
   *
   * @param string $entity_type
   *   Entity type.
   * @param array $values
   *   Values.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity.
   */
  protected function createEntity($entity_type, array $values) {
    $storage = $this->entityTypeManager->getStorage($entity_type);
    $entity = $storage->create($values);
    $entity->enforceIsNew();
    $storage->save($entity);

    return $entity;
  }

  /**
   * Create a node.
   *
   * @param string $type
   *   Node type.
   *
   * @return \Drupal\node\NodeInterface
   *   Node entity.
   */
  protected function createNode($type) {
    return $this->createEntity('node', [
      'title' => $this->randomString(),
      'body' => $this->randomString(),
      'type' => $type,
      'uid' => 1,
    ]);
  }

  /**
   * Create a node type.
   *
   * @return \Drupal\node\Entity\NodeType
   *   The created node type entity.
   */
  protected function createNodeType($type) {
    return $this->createEntity('node_type', [
      'type' => $type,
      'label' => $this->randomString(),
    ]);
  }

  /**
   * Removes entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to be removed.
   */
  protected function removeEntity(EntityInterface $entity) {
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $storage->delete([$entity]);
  }

}
