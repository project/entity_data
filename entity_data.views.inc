<?php

/**
 * @file
 * Hooks for entity_data.views.inc.
 */

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Implements hook_views_data().
 */
function entity_data_views_data() {
  $data = [];

  // Define the base group of this table. Fields that don't have a group defined
  // will go into this field by default.
  $data['entity_data']['table']['group'] = t('Entity data');

  $entities_types = \Drupal::entityTypeManager()->getDefinitions();
  foreach ($entities_types as $type => $entity_type) {
    if (!$entity_type->entityClassImplements(ContentEntityInterface::class) || !$entity_type->getBaseTable()) {
      continue;
    }

    $data['entity_data']['table']['join'][$entity_type->getDataTable() ?: $entity_type->getBaseTable()] = [
      'type' => 'LEFT',
      'left_field' => $entity_type->getKey('id'),
      'field' => 'entity_id',
      'extra' => [
        [
          'field' => 'entity_type',
          'value' => $type,
        ],
      ],
    ];
  }

  $data['entity_data']['value'] = [
    'title' => t('Entity data: value'),
    'help' => t('Entity data value of data.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['entity_data']['module'] = [
    'title' => t('Entity data: module'),
    'help' => t('Entity data module which stores data.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  $data['entity_data']['name'] = [
    'title' => t('Entity data: name'),
    'help' => t('Entity data name of data.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'argument' => [
      'id' => 'string',
    ],
  ];

  return $data;
}
