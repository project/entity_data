### About

This module allows storing custom data in any format per entity.
For example:
- Notification settings per entity
- Social media available per entity
- Moderation settings per entity
- etc.

This task can be solved by using fields for example or other modules, but entity data module allows storing scalar data
as well as the complex data structures like arrays or objects.
Another advantage it has a super simple API and you don't have to pollute your content entities with many fields.

### Installation

1) composer require drupal/entity_data

2) drush en entity_data

### Configuration

We can store objects in entity_data, but to allow it you need to explicitly allow classes we are going to handle.
You can allow classes and define a list of allowed here:

/admin/config/development/entity-data

### Usage

This module does not require any configuration. To manipulate data you need to use **'entity.data'** service

#### Store data

Template:
```
\Drupal::service('entity.data')->set('[YOUR_MODULE]', [ENTITY_ID], '[NAME_OF_CUSTOM_DATA]', '[ENTITY_TYPE]', [VALUE]);
```

Example:
```
\Drupal::service('entity.data')->set('ex_group', 1, 'send_notifications', 'group', TRUE);
```

#### Fetch data

Template:
```
\Drupal::service('entity.data')->get('[YOUR_MODULE]', [ENTITY_ID], '[NAME_OF_CUSTOM_DATA]', '[ENTITY_TYPE]');
```

Example:
```
$send_notidications = \Drupal::service('entity.data')->get('ex_group', 1, 'send_notifications', 'group');
```

#### Delete data

Template:
```
\Drupal::service('entity.data')->delete('[YOUR_MODULE]', [ENTITY_ID], '[NAME_OF_CUSTOM_DATA]', '[ENTITY_TYPE]');
```

Example:
```
\Drupal::service('entity.data')->delete('ex_group', 1, 'send_notifications', 'group');
```


### Maintainers

* Nikolay Lobachev https://www.drupal.org/u/lobsterr
