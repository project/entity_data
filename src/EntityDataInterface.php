<?php

namespace Drupal\entity_data;

/**
 * Defines the EntityDataInterface data service interface.
 */
interface EntityDataInterface {

  /**
   * Returns data stored for an entity.
   *
   * @param string $module
   *   The name of the module the data is associated with.
   * @param int $entity_id
   *   (optional) The entity ID the data is associated with.
   * @param string $name
   *   (optional) The name of the data key.
   * @param string $entity_type
   *   (optional) The entity_type of the data key.
   *
   * @return mixed|array
   *   The requested entity data, depending on the arguments passed:
   *   - For $module, $name, and $entity_id, the stored value is returned,
   *     or NULL if no value was found.
   *   - For $module and $entity_id, an associative array is returned that
   *     contains the stored data name/value pairs.
   *   - For $module and $name, an associative array is returned whose keys are
   *     entities IDs and whose values contain the stored values.
   *   - For $module only, an associative array is returned that contains all
   *     existing data for $module in all entities, keyed first by entity ID
   *     and $name second.
   */
  public function get($module, $entity_id = NULL, $name = NULL, $entity_type = NULL);

  /**
   * Stores data for an entity.
   *
   * @param string $module
   *   The name of the module the data is associated with.
   * @param int $entity_id
   *   The entity ID the data is associated with.
   * @param string $name
   *   The name of the data key.
   * @param string $entity_type
   *   (optional) The entity_type of the data key.
   * @param mixed $value
   *   The value to store. Non-scalar values are serialized automatically.
   */
  public function set($module, $entity_id, $name, $entity_type, $value);

  /**
   * Deletes data stored for an entity.
   *
   * @param string|array $module
   *   (optional) The name of the module the data is associated with. Can also
   *   be an array to delete the data of multiple modules.
   * @param int|array $entity_id
   *   (optional) The entity ID the data is associated with. If omitted,
   *   all data for $module is deleted. Can also be an array of IDs to delete
   *   the data of multiple entities.
   * @param string $name
   *   (optional) The name of the data key. If omitted, all data associated with
   *   $module and $entity_id is deleted.
   * @param string $entity_type
   *   (optional) The entity_type of the data key.
   */
  public function delete($module = NULL, $entity_id = NULL, $name = NULL, $entity_type = NULL);

}
