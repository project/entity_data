<?php

namespace Drupal\entity_data\Plugin\views\field;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\entity_data\EntityDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides access to the entity data service.
 *
 * @ingroup views_field_handlers
 *
 * @see \Drupal\entity_data\EntityDataInterface
 *
 * @ViewsField("entity_data")
 */
class EntityData extends FieldPluginBase {

  /**
   * Provides the entity data service object.
   *
   * @var \Drupal\entity_data\EntityDataInterface
   */
  protected $entityData;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity.data'),
      $container->get('module_handler'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a EntityData object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityDataInterface $entity_data, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityData = $entity_data;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['entity_data_module'] = ['default' => ''];
    $options['entity_data_name'] = ['default' => ''];
    $options['entity_data_entity_type'] = ['default' => ''];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $modules = $this->moduleHandler->getModuleList();
    $names = [];
    foreach (array_keys($modules) as $name) {
      $names[$name] = $this->moduleHandler->getName($name);
    }

    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $entity_types = [];
    foreach ($entity_definitions as $key => $entity_definition) {
      $entity_types[$key] = $entity_definition->getLabel();
    }

    $form['entity_data_name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('The name of the data key.'),
      '#default_value' => $this->options['entity_data_name'],
    ];

    $form['entity_data_module'] = [
      '#title' => $this->t('Module name'),
      '#type' => 'select',
      '#description' => $this->t('The module which sets this entity data.'),
      '#default_value' => $this->options['entity_data_module'],
      '#options' => $names,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity_id = $this->getValue($values);
    $data = $this->entityData->get($this->options['entity_data_module'], $entity_id, $this->options['entity_data_name'], $values->_entity->getEntityType()->id());

    // Don't sanitize if no value was found.
    if (!empty($data)) {
      return $this->sanitizeValue($data);
    }
  }

}
