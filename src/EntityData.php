<?php

namespace Drupal\entity_data;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;

/**
 * Defines the entity data service.
 */
class EntityData implements EntityDataInterface {

  /**
   * The database connection to use.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Immutable configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new entity data service.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection to use.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory.
   */
  public function __construct(Connection $connection, ConfigFactoryInterface $configFactory) {
    $this->connection = $connection;
    $this->config = $configFactory->get('entity_data.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function get($module, $entity_id = NULL, $name = NULL, $entity_type = NULL) {
    $query = $this->connection->select('entity_data', 'ed')
      ->fields('ed')
      ->condition('module', $module);

    if (!empty($entity_id)) {
      $query->condition('entity_id', $entity_id);
    }

    if (!empty($name)) {
      $query->condition('name', $name);
    }

    if (!empty($entity_type)) {
      $query->condition('entity_type', $entity_type);
    }

    $result = $query->execute();

    // If $module, $entity_id, and $name were passed, return the value.
    if (isset($name) && isset($entity_id) && isset($entity_type)) {
      $result = $result->fetchAllAssoc('entity_id');
      if (isset($result[$entity_id])) {
        return $this->getValue($result[$entity_id]);
      }
      return NULL;
    }

    $return = [];
    // If $module and $entity_id were passed, return data keyed by name.
    if (isset($entity_id) && isset($entity_type)) {
      foreach ($result as $record) {
        $return[$record->name] = $this->getValue($record);
      }
      return $return;
    }

    // If $module and $name were passed, return data
    // keyed by entity_type / entity_id.
    if (isset($name)) {
      foreach ($result as $record) {
        $return[$record->entity_type][$record->entity_id] = $this->getValue($record);
      }
      return $return;
    }

    // If $module and $entity_id were passed, return data
    // keyed by entity_type / name.
    if (isset($entity_id)) {
      foreach ($result as $record) {
        $return[$record->entity_type][$record->name] = $this->getValue($record);
      }
      return $return;
    }

    // If $module and $entity_id were passed, return data
    // keyed by entity_id / name.
    if (isset($entity_type)) {
      foreach ($result as $record) {
        $return[$record->entity_id][$record->name] = $this->getValue($record);
      }
      return $return;
    }

    // If only $module was passed, return data keyed by entity_id and name.
    foreach ($result as $record) {
      $return[$record->entity_type][$record->entity_id][$record->name] = $this->getValue($record);
    }

    return $return;
  }

  /**
   * Get record value.
   *
   * @param \StdClass $record
   *   Record received from database.
   *
   * @return mixed
   *   Stored data.
   */
  protected function getValue(\StdClass $record) {
    // Make sure we unserialize only allowed classes.
    // @see https://www.php.net/manual/en/function.unserialize.php.
    if ($record->serialized) {
      $allowed_classes = FALSE;
      if ($this->config->get('are_classes_allowed')) {
        $allowed_classes = $this->config->get('allowed_classes');
      }

      return unserialize($record->value, ['allowed_classes' => $allowed_classes]);
    }
    else {
      return $record->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function set($module, $entity_id, $name, $entity_type, $value) {
    $serialized = (int) !is_scalar($value);
    if ($serialized) {
      $value = serialize($value);
    }
    $this->connection->merge('entity_data')
      ->keys([
        'entity_id' => $entity_id,
        'module' => $module,
        'entity_type' => $entity_type,
        'name' => $name,
      ])
      ->fields([
        'value' => $value,
        'serialized' => $serialized,
      ])
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function delete($module = NULL, $entity_id = NULL, $name = NULL, $entity_type = NULL) {
    $query = $this->connection->delete('entity_data');

    // Cast scalars to array so we can consistently use an IN condition.
    if (!empty($module)) {
      $query->condition('module', (array) $module, 'IN');
    }

    if (!empty($entity_id)) {
      $query->condition('entity_id', (array) $entity_id, 'IN');
    }

    if (!empty($name)) {
      $query->condition('name', (array) $name, 'IN');
    }

    if (!empty($entity_type)) {
      $query->condition('entity_type', (array) $entity_type, 'IN');
    }

    $query->execute();
  }

}
