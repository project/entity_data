<?php

namespace Drupal\entity_data\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a setting UI for entity data.
 *
 * @package Drupal\entity_data\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'entity_data.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_data_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Request $request = NULL) {
    $description = $this->t('<a href="https://www.php.net/manual/en/function.unserialize.php">PHP unserialize</a> function is not safe to use when we work with user input .<br />
One configuration name per line.<br />
Examples: <ul>
<li>Drupal\node\Entity\Node</li>
<li>Drupal\taxonomy\Entity\Term</li>
<li>Drupal\group\Entity\Group</li>
</ul>');

    $entity_data_settings = $this->config('entity_data.settings');
    $form['are_classes_allowed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is classes allowed?'),
      '#default_value' => $entity_data_settings->get('are_classes_allowed'),
      '#description' => $this->t('If you are going to use classes, you need explicitly define them bellow.'),
    ];
    $form['allowed_classes'] = [
      '#type' => 'textarea',
      '#rows' => 25,
      '#title' => $this->t('Allowed classes'),
      '#description' => $description,
      '#default_value' => implode(PHP_EOL, $entity_data_settings->get('allowed_classes')),
      '#size' => 60,
      '#states' => [
        'disabled' => [
          ':input[name="are_classes_allowed"]' => ['checked' => FALSE],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $allowed_classes = $form_state->getValue('allowed_classes');
    $are_classes_allowed = $form_state->getValue('are_classes_allowed');
    $entity_data_settings = $this->config('entity_data.settings');
    $entity_data_settings_array = preg_split("/[\r\n]+/", $allowed_classes);
    $entity_data_settings_array = array_filter($entity_data_settings_array);
    $entity_data_settings_array = array_values($entity_data_settings_array);
    $entity_data_settings->set('allowed_classes', $entity_data_settings_array);
    $entity_data_settings->set('are_classes_allowed', $are_classes_allowed);
    $entity_data_settings->save();
    parent::submitForm($form, $form_state);
  }

}
