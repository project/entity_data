<?php

/**
 * @file
 * Install, update and uninstall functions for the entity_data module.
 */

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Implements hook_schema().
 */
function entity_data_schema() {
  $schema['entity_data'] = [
    'description' => 'Stores module data as key/value pairs per entity.',
    'fields' => [
      'entity_id' => [
        'description' => 'Entity ID.',
        'type' => 'varchar_ascii',
        'length' => EntityTypeInterface::ID_MAX_LENGTH,
        'not null' => TRUE,
        'default' => '',
      ],
      'module' => [
        'description' => 'The name of the module declaring the variable.',
        'type' => 'varchar_ascii',
        'length' => DRUPAL_EXTENSION_NAME_MAX_LENGTH,
        'not null' => TRUE,
        'default' => '',
      ],
      'entity_type' => [
        'description' => 'The entity type.',
        'type' => 'varchar_ascii',
        'length' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
        'not null' => TRUE,
        'default' => '',
      ],
      'name' => [
        'description' => 'The identifier of the data.',
        'type' => 'varchar_ascii',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
      ],
      'value' => [
        'description' => 'The value.',
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
      ],
      'serialized' => [
        'description' => 'Whether value is serialized.',
        'type' => 'int',
        'size' => 'tiny',
        'unsigned' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['entity_id', 'module', 'name', 'entity_type'],
    'indexes' => [
      'module' => ['module'],
      'name' => ['name'],
      'entity_type' => ['entity_type'],
    ],
  ];

  return $schema;
}

/**
 * Set default settings.
 */
function entity_data_update_9301() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('entity_data.settings');
  $config->set('are_classes_allowed', FALSE);
  $config->set('allowed_classes', []);
  $config->save();
}
